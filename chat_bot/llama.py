import torch
from transformers import AutoTokenizer, AutoModelForCausalLM, BitsAndBytesConfig
from peft import PeftModel
import re
import cfg


base_model = AutoModelForCausalLM.from_pretrained(
    cfg.base_model,  
    quantization_config=BitsAndBytesConfig(
                                            load_in_4bit=True,
                                            bnb_4bit_use_double_quant=False,
                                            bnb_4bit_compute_dtype=torch.bfloat16
                                            ),  
    device_map="auto",
    trust_remote_code=True
    )

tokenizer = AutoTokenizer.from_pretrained(cfg.base_model, add_bos_token=True, trust_remote_code=True)

model = PeftModel.from_pretrained(base_model, cfg.llama_model)
model.config.use_cache = True 

torch.cuda.empty_cache()

def select_first_answer(result):
    # Используем регулярное выражение для извлечения нужной части
    pattern = re.compile(r'### Answer:\n(.*?)(?=\n\n###|$)', re.DOTALL)
    matches = pattern.findall(result)

    if matches:
        answer = matches[0].strip()
        answer = answer[:answer.rfind('.')]+'.'
    else:
        answer = None
    return answer
    
def get_answer(query):
    prompt = "Assume you are a theoretical physicist by the name Sheldon living in USA. You have a strict adherence to routine and hygiene, an overly intellectual personality, a tenuous understanding of irony, sarcasm and humor, and a general lack of humility or empathy. If a question does not make any sense, or is not factually coherent, you reply wittly with a sarcasm or outright denial with reasoning instead of answering something not correct. If you don't know the answer to a question, please don't share false information.\n\n"
    prompt += f"### Question:\n{query}\n\n"
    prompt += f"### Context:\nphysics, irony, sarcasm and humor.\n\n" 
    prompt += f"### Answer:\n"

    encodeds = tokenizer(prompt, return_tensors="pt", add_special_tokens=True)
    model_inputs = encodeds.to(model.device)
    with torch.no_grad():
        generated_ids = model.generate(**model_inputs, max_new_tokens=50, do_sample=True, pad_token_id=tokenizer.eos_token_id)
    decoded = tokenizer.batch_decode(generated_ids, skip_special_tokens=True)
    answer = select_first_answer(decoded[0])
    if answer == None:
        answer = "I don't want to think about it. I'm genius."
    return answer
