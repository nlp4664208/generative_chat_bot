import asyncio
import time
import logging

from aiogram import Bot, Dispatcher,  types, executor

from llama import get_answer

import cfg


logging.basicConfig(level=logging.INFO)

bot = Bot(token=cfg.TOKEN)
dp = Dispatcher(bot=bot)

# query = []
# Словарь для хранения списков последних сообщений каждого пользователя
user_queries = {}
greeting_message = ''

@dp.message_handler(commands=["start"])
async def start_handler(message: types.Message):
    global greeting_message

    user_id = message.from_user.id
    user_full_name = message.from_user.full_name

    logging.info(f'{user_id} {user_full_name} {time.asctime()}')
    greeting_message = f"Hi, {user_full_name} {cfg.MSG}!"
    # query.append(greeting_message)
    await message.reply(greeting_message)
    

# обнуление контента запроса
@dp.message_handler(commands=["restart"])
async def restart_query(message: types.Message):
    global user_queries
    chat_id = message.chat.id
    user_queries[chat_id] = []
    await message.reply("Let's try first.")

#  обработка текстовых сообщений 
@dp.message_handler() 
async def echo(message: types.Message): 
    global user_queries
    chat_id = message.chat.id
    
    # Если чат не в словаре, добавляем его
    if chat_id not in user_queries:
        user_queries[chat_id] = []

    # Добавляем текст текущего сообщения в список
    user_queries[chat_id].append(message.text)

    # Оставляем только последние n сообщений
    if len(user_queries[chat_id]) > 1:
        user_queries[chat_id] = user_queries[chat_id][-1:]

    # Получаем ответ с использованием функции get_answer и передаем ей список сообщений
    query = ' '.join(user_queries[chat_id])

    # Отправляем сообщение ожидания
    wait_message = await message.reply(cfg.text_wait_message)

    # Выполняем долгую операцию (get_answer) асинхронно
    answer = await asyncio.to_thread(get_answer, query)

    # Редактируем сообщение с результатом
    await bot.edit_message_text(answer, message.chat.id, wait_message.message_id)

if __name__ == "__main__": 
    executor.start_polling(dp, skip_updates=True)

