# Chat Bot

## Описание

#### Чат-бот TestNlPChat (@testnlp_hw_bot) работает на базе месенджера Telegram.
В качестве модели для создания чата взята большая языковая модель LLaMA7b (Large Language Model Meta AI), над которой потом произведена тонкая настройка (fine-tuning) c использованием LoRA (Low-Rank Adaptation). 

В качестве попытки борьбы над галлюционированием модели по мере генерации был добавлен контекст, содержащий информацию о общей направленности ответов нашего персонажа (RAG). 

## Данные

Набор данных взят из субтитров сериала "Теория большого взрыва" на английском языке, доступных по [ссылке](https://www.kaggle.com/datasets/mitramir5/the-big-bang-theory-series-transcript?select=1_10_seasons_tbbt.csv).

## Директория

### `train_and_inference` - файлы для обучение и инференс моделей:
- `train.ipynb` – обучение модели
- `inference.ipynb` – инференс модели
- `dataset.ipynb` – подготовка датасета для обучения
- **data:**
  - `1_10_seasons_tbbt.csv` – исходный датасет
  - `outputs.csv` – датасет для обучения 
- **model:** (скачать по [ссылке](https://drive.google.com/drive/folders/11V8qH8swc_yQPjosilvMGRoftz2M3NEI?usp=share_link))
  - `checkpoint-200`
- `requirements.txt`

### `generative_chat_bot` - файлы для работы чат-бота:
- `bot.py` – главный файл программы, из которого чат запускается
- `llama.py` – реализация вызова модели llama
- `cfg.py` – конфигурационный файл
- **model:** (скачать по [ссылке](https://drive.google.com/drive/folders/11V8qH8swc_yQPjosilvMGRoftz2M3NEI?usp=share_link))
  - `checkpoint-200`
- `requirements.txt`


    
